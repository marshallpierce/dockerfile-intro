FROM alpine:3.9 AS jvm

RUN mkdir -p /usr/local/zulu \
    && wget https://cdn.azul.com/zulu/bin/zulu11.29.3-ca-jdk11.0.2-linux_musl_x64.tar.gz -O - \
    | tar -xz -f - -C /usr/local/zulu

ENV JAVA_HOME=/usr/local/zulu/zulu11.29.3-ca-jdk11.0.2-linux_musl_x64
ENV PATH $PATH:$JAVA_HOME/bin

FROM jvm AS build

ARG BUILD_SECRETS

RUN echo "got some secrets:" $BUILD_SECRETS

RUN mkdir /app
WORKDIR /app

# copying directories flattens them, so you have to specify destination
COPY /gradle ./gradle
COPY /gradlew ./

# get gradle itself downloaded so it can be separately cached
RUN ./gradlew --version

COPY /build.gradle.kts /settings.gradle.kts ./
# cache compiled build setup
RUN ./gradlew tasks > /dev/null

COPY /src ./src

RUN ./gradlew distTar

RUN tar -xf build/distributions/dockerfile-intro.tar

FROM jvm

ARG COMMIT_ID

WORKDIR /app

ENTRYPOINT ["/app/dockerfile-intro/bin/dockerfile-intro"]
CMD ["foo", "bar"]

COPY --from=build /app/dockerfile-intro /app/dockerfile-intro

# label comes last because it changes often and is cheap
LABEL com.classpass.vcs.commit_id=$COMMIT_ID
